const { GraphQLServer } = require('graphql-yoga');
const path = require('path');
const resolvers = require('./resolvers');
const mongoose = require('mongoose');

// typeDefs = definição de tipo. Devemos informar o SCHEMA

mongoose.connect('mongodb+srv://admin:fateChs2019@dev-fate-chs-by25f.azure.mongodb.net/graphqlnode?retryWrites=true&w=majority',{
    useNewUrlParser: true
})

const server = new GraphQLServer({
    typeDefs: path.resolve(__dirname, 'schema.graphql'),
    resolvers
});

// resolvers = como se fossem as rotas do REST

server.start();
